require 'breakpoint'
require 'susy'

http_path = "/"
css_dir = "css"
sass_dir = "sass"
images_dir = "img"
javascripts_dir = "js"
fonts_dir = "fonts"

output_style = :compact

relative_assets = true

line_comments = false

preferred_syntax = :sass