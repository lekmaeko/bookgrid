# bookgrid

This is the bookgrid CSS/SASS framework for setting beautiful books. It's meant to work just as well with HTML-based publications as with ePub and/or Kindle. Just include the CSS in the ePub and fire
away.

Maybe the "grid" part of the name is a bit unfortunate, I mean, do you really need a grid for a book? This is nontheless a CSS grid, responsive and with support for multiple columns, aside, figures and all that.

### Usage

To get started,

    bundle install

and

    compass compile

Then copy the CSS into your publication.

Or embed this into your project by copying the ``Gemfile``, ``sass`` directory and ``config.rb`` there. Whatever floats your boat.

## License

This work is licensed under the MIT license.